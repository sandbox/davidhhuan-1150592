-- LICENSE --
MIT license

-- SUMMARY --
This module is used to get the address via IP in China. 
For example, 220.242.75.108 is a IP of 广东省江门市 视讯宽带
The qqwry.dat is downloaded from the Internet, from its license, we can use this data file for free without any limit.
More details about this data file, please go to http://www.cz88.net

1.0 - beta
The version of qqwry.dat is 20110505


-- REQUIREMENTS --
Drupal 7.x


-- INSTALLATION --

* Install as usual

* Go to http://your_website/bm/ip , you can find this functionality.


-- CONTACT --

Current maintainers:
* Davidhhuan (David Lee) - http://www.cnblogs.com/davidhhuan
Just feel free to contact me: davidhhuan@gmail.com
